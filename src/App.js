import {useState} from 'react'

import './App.css'

import Home from 'pages/home'
import Register from 'pages/register'
import Login from 'pages/login'
import Logout from 'pages/logout'
import AddCategories from 'pages/addCategory'
import Categories from 'pages/categories'
import Expenses from 'pages/expenses'
import Income from 'pages/income'

import NavBar from 'components/NavBar'

import {Container} from 'react-bootstrap'
import {BrowserRouter as Router} from 'react-router-dom'
import {Route,Switch} from 'react-router-dom'

import {UserProvider} from './userContext'

function App(){

  const [user,setUser] = useState({

    email: localStorage.getItem('email'),
  })

  console.log(user)

  const unsetUser = () => {

    localStorage.clear()

  }

   return (
  <>  
    <UserProvider value={{user, setUser,unsetUser}}>
      <Router>
        <NavBar />
        <Container>
          <Switch>
           <Route exact path="/" component={Home}/>
            <Route exact path="/register" component={Register}/>
            <Route exact path="/login" component={Login}/>
            <Route exact path="/logout" component={Logout}/>
            <Route exact path="/addcategories" component={AddCategories}/>
            <Route exact path="/categories" component={Categories}/>
            <Route exact path="/expenses" component={Expenses}/>
            <Route exact path="/income" component={Income}/>
          </Switch>
        </Container>
      </Router>
    </UserProvider>  
  </>
  );  


}

export default App;
