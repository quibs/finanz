import React,{useState,useEffect,useContext} from 'react'

import Category from 'components/Category'

import UserContext from 'userContext'

export default function Categories(){

	const {user} = useContext(UserContext)
	console.log(user)

	const [newCategories,setNewCategories] = useState([])

		useEffect(()=>{

			fetch('https://nameless-brook-20939.herokuapp.com/api/categories/',{
				
				headers: {
						Authorization: `Bearer ${localStorage.getItem('token')}`
					}

			})

			.then(res => res.json())
			.then(data => {

				// console.log(data)

				setNewCategories(data)

			})
			// console.log(newCategories)
		},[])


	let categoryComponents = newCategories.map(category => {

  		return <Category key={category._id} category={category}/>

	})
  
  	// console.log(categoryComponents)

	return (
		user.email
		?
		<>
			<h1 className="text-center mb-5">Categories</h1>
			{categoryComponents}
		</>
		:
		window.location.href="/login"

		)


}

