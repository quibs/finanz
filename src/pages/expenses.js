import React,{useState,useEffect, useContext} from 'react'
import {Form,Table,Button,Row,Col} from 'react-bootstrap'

import Swal from 'sweetalert2'

import UserContext from 'userContext'

export default function Expenses(){

	const {user} = useContext(UserContext)
	console.log(user)

	const [expenses,setExpenses] = useState([])
	const [message,setMessage] = useState({})

		useEffect(()=>{

			fetch('https://nameless-brook-20939.herokuapp.com/api/entries/expenses',{

				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}

			})
			.then(res => res.json())
			.then(data => {

				console.log(data)
				setExpenses(data)

			})

		},[message])	
		console.log(expenses)

	let expenseComponents = expenses.map(expense => {

		console.log(expense)

  		return (

  				<tr>
					<td>{expense.category}</td>
					<td>PHP {expense.amount.toFixed(2)}</td>
					<td><Button variant="danger" onClick={()=>deleteExpense(expense._id)} id="deleteButton">Delete</Button></td>
				</tr>

  			)

	})

	let expenseSum = expenses.reduce(function (total, currentValue){
		return total + currentValue.amount
	}, 0)

	const [categories,setCategories] = useState([])

		useEffect(()=>{

			fetch('https://nameless-brook-20939.herokuapp.com/api/categories/',{
				
				headers: {
						Authorization: `Bearer ${localStorage.getItem('token')}`
					}

			})

			.then(res => res.json())
			.then(data => {

				// console.log(data)

				setCategories(data.filter(category=> {
					return category.type === "expense"

				}).map(category=>{
					return <option key={category._id} value={category.name}>{category.name}</option>
				}))
			})
			
		},[])


	const [category,setCategory] = useState("")
	const [amount,setAmount] = useState("")
	const [type,setType] = useState("")

	const [isActive, setIsActive]= useState(true)

	useEffect(()=>{

		if(category !== "" && amount !== "" && type !== ""){

			setIsActive(true)

		} else {

			setIsActive(false)

		}

	},[category,amount,type])	

	function addExpense (e){

		e.preventDefault()

			fetch('https://nameless-brook-20939.herokuapp.com/api/entries/',{

				method: 'POST',
				headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`,
				'Content-Type': 'application/json'
				},
				body:JSON.stringify({
					category: category,
					amount: amount,
					type: type	
				})

			})
			.then(res => res.json())
			.then(data => {

				console.log(data)
				setMessage(data)
				Swal.fire({

				icon: "success",
				title: "successfully added",

				})
			})

		
			setCategory("")
			setAmount("")
			setType("")

	}

	function deleteExpense(expense){

		console.log(expense)

		fetch('https://nameless-brook-20939.herokuapp.com/api/entries/' + expense ,{

			method: 'DELETE',
			headers: {
						Authorization: `Bearer ${localStorage.getItem('token')}`
					}

			})
			.then(res => res.json())
			.then(data => {

				console.log(data)
				setMessage(data)

				// window.location.href="/expenses"
			
			})



		
	}	

	return (
		user.email
		?
		<>
			<Row id="expensepage">
				<Col xs={12} md={8}>
					<>
						<h2 className="text-center mb-5" >Expenses Table</h2>
						<Table responsive id ="Table">
							<thead>
								<tr>
									<th>Category</th>
									<th>Amount: PHP {expenseSum.toFixed(2)}</th>
									<th>Delete</th>
								</tr>
							</thead>
							<tbody>
								{expenseComponents}
							</tbody>
						</Table>
					</>
				</Col>
				<Col xs={6} md={4}>
					<h3 className="text-center mb-5" style={{fontSize: 34}}>Expenses Form</h3>
					<Form onSubmit={e => addExpense(e)}>
					<Form.Group>
						<Form.Label>Category</Form.Label>
							<Form.Control as="select" placeholder="Select" value={category} onChange={e=>{setCategory(e.target.value)}}required>
								<option value="" disabled>Select</option>
								{categories}
							</Form.Control>	
					</Form.Group>
					<Form.Group>
						<Form.Label>Amount</Form.Label>
						<Form.Control type="number" placeholder="Enter Amount" value={amount} onChange={e=>{setAmount(e.target.value)}}required />
					</Form.Group>
					<Form.Group>
							<Form.Label>Type</Form.Label>
							<Form.Control as="select" placeholder="Select" value={type} onChange={e=>{setType(e.target.value)}} required>
								<option value="">Select</option>
								<option value="expense">expense</option>
							</ Form.Control>
					</Form.Group>
					{
						isActive
						?
						<Button variant="primary" type="submit">Submit</Button>
						:
						<Button variant="primary" type="submit" disabled>Submit</Button>
					}
				</Form>
				</Col>
			</Row>
		</>
		:
		window.location.href="/login"
	)		

}