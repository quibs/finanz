import {useContext} from 'react'
import Banner from 'components/Banner'

import UserContext from 'userContext'


export default function Home(){

	// console.log(useContext(UserContext))

	const {user} = useContext(UserContext)
	console.log(user)

	let banner = {

    title: "Finanz",
    description: "Track your finances",
    motto: "LET'S SAVE SOME MONEY!",
    label: "Register Here"

  }

	return (
		user.email
		?
			window.location.href="/addcategories"
      	:
		<>
			<Banner dataProp={banner} />
      	</>
		)

}