import React,{useState,useEffect} from 'react'
import {Form, Button} from 'react-bootstrap'

import {Redirect} from 'react-router-dom'

import Swal from 'sweetalert2'

export default function Register(){

	const [firstName,setFirstName] = useState("")
	const [lastName,setLastName] = useState("")
	const [email,setEmail] = useState("")
	const [password,setPassword] = useState("")
	const [confirmPassword,setConfirmPassword] = useState("")

	const [isActive, setIsActive]= useState(true)

	const [willRedirect,setWillRedirect] = useState(false)

	useEffect(()=>{

		if((firstName !== "" && lastName !== "" && email !== "" && password !== "" && confirmPassword !== "") && (password === confirmPassword)){

			setIsActive(true)

		} else {

			setIsActive(false)

		}

	},[firstName,lastName,email,password,confirmPassword])

	function registerUser (e){

		e.preventDefault()

		// alert("Thank you for registering")


		fetch('https://nameless-brook-20939.herokuapp.com/api/users/', {

			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body:JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				password: password,
				confirmPassword: confirmPassword	
			})

		})
		.then(res => res.json())
		.then(data => {

				console.log(data)

				/*if(data === {error: "Email already exists"}){*/
					Swal.fire({

					icon: "success",
					title: "You are now registered!",
					text: "Thank you for registering to Zuitt Booking System"

					})

					setWillRedirect(true)
					
				/*} else {
					Swal.fire({
						icon:"error",
						title:"Registration failed",
						text:"Email is already registered"
					})*/
				
			/*}*/
		})

		setFirstName("")
		setLastName("")
		setEmail("")
		setPassword("")
		setConfirmPassword("")

	}

	return (
		willRedirect
		?
		<Redirect to="/login"/>
		:
		<>
		<div id="register">
			<h3 className="text-center" style={{fontSize: 34}}>Register</h3>
			
			<Form onSubmit={e => registerUser(e)}>
				<Form.Group>
					<Form.Label>First Name</Form.Label>
					<Form.Control type="text" placeholder="Enter First Name" value={firstName} onChange={e=>{setFirstName(e.target.value)}}required />
				</Form.Group>
				<Form.Group>
					<Form.Label>Last Name</Form.Label>
					<Form.Control type="text" placeholder="Enter Last Name" value={lastName} onChange={e=>{setLastName(e.target.value)}} required />
				</Form.Group>
				<Form.Group>
					<Form.Label>Email</Form.Label>
					<Form.Control type="email" placeholder="Enter Email" value={email} onChange={e=>{setEmail(e.target.value)}} required />
				</Form.Group>
				<Form.Group>
					<Form.Label>Password</Form.Label>
					<Form.Control type="password" placeholder="Must be 8 characters or more." value={password} onChange={e=>{setPassword(e.target.value)}} required />
				</Form.Group>
				<Form.Group>
					<Form.Label>Confirm Password</Form.Label>
					<Form.Control type="password" placeholder="Must be the same as password" value={confirmPassword} onChange={e=>{setConfirmPassword(e.target.value)}} required />
				</Form.Group>
				{
					isActive
					?
					<Button variant="primary" type="submit">Register</Button>
					:
					<Button variant="primary" type="submit" disabled>Register</Button>
				}
			</Form>
		</div>
		</>
		)


}