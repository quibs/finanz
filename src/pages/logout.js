import React,{useContext} from 'react'
import UserContext from 'userContext'

import {Redirect} from 'react-router-dom'

export default function Logout(){

		const {user,setUser,unsetUser} = useContext(UserContext)
		console.log(user)
		console.log(setUser)
		console.log(unsetUser)
	
	unsetUser()

	setUser({
		email:null,
		isAdmin:null
	})

	return(

		<Redirect to="/" />

	)

}