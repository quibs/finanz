import React,{useState,useEffect, useContext} from 'react'
import {Form,Table,Button,Row,Col} from 'react-bootstrap'

import Swal from 'sweetalert2'

import UserContext from 'userContext'

export default function Income(){

	const {user} = useContext(UserContext)
	console.log(user)

	const [income,setIncome] = useState([])
	const [message,setMessage] = useState({})

		useEffect(()=>{

			fetch('https://nameless-brook-20939.herokuapp.com/api/entries/income',{

				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}

			})
			.then(res => res.json())
			.then(data => {

				// console.log(data)
				setIncome(data)

			})

		},[message])	
		console.log(income)

	let incomeComponents = income.map(income => {

  		return (

  				<tr>
					<td>{income.category}</td>
					<td>PHP {income.amount.toFixed(2)}</td>
					<td><Button variant="danger" onClick={()=>deleteIncome(income._id)} id="deleteButton">Delete</Button></td>
				</tr>

  			)

	})

	let incomeSum = income.reduce(function (total, currentValue){
		return total + currentValue.amount
	}, 0)

	const [categories,setCategories] = useState([])

		useEffect(()=>{

			fetch('https://nameless-brook-20939.herokuapp.com/api/categories/',{
				
				headers: {
						Authorization: `Bearer ${localStorage.getItem('token')}`
					}

			})

			.then(res => res.json())
			.then(data => {

				console.log(data)

				setCategories(data.filter(category=> {
					return category.type === "income"

				}).map(category=>{
					return <option key={category._id} value={category.name}>{category.name}</option>
				}))
			})
			
		},[])


	console.log(categories)

	const [category,setCategory] = useState("")
	const [amount,setAmount] = useState("")
	const [type,setType] = useState("")

	const [isActive, setIsActive]= useState(true)

	useEffect(()=>{

		if(category !== "" && amount !== "" && type !== ""){

			setIsActive(true)

		} else {

			setIsActive(false)

		}

	},[category,amount,type])	

	function addIncome (e){

		e.preventDefault()

			fetch('https://nameless-brook-20939.herokuapp.com/api/entries/',{

				method: 'POST',
				headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`,
				'Content-Type': 'application/json'
				},
				body:JSON.stringify({
					category: category,
					amount: amount,
					type: type	
				})

			})
			.then(res => res.json())
			.then(data => {

				console.log(data)
				setMessage(data)
				Swal.fire({

				icon: "success",
				title: "successfully added",

				})
			})

		
			setCategory("")
			setAmount("")
			setType("")

	}	

	function deleteIncome(income){

		console.log(income)

		fetch('https://nameless-brook-20939.herokuapp.com/api/entries/' + income ,{

			method: 'DELETE',
			headers: {
						Authorization: `Bearer ${localStorage.getItem('token')}`
					}

			})
			.then(res => res.json())
			.then(data => {

				console.log(data)
				setMessage(data)
			
			})


		
	}	

	return (
		user.email
		?
		<>
			<Row id="incomepage">
				<Col xs={12} md={8}>
					<>
						<h2 className="text-center mb-5">Income Table</h2>
						<Table responsive id ="Table">
							<thead>
								<tr>
									<th>Category</th>
									<th>Amount: PHP {incomeSum.toFixed(2)}</th>
									<th>Delete</th>
								</tr>
							</thead>
							<tbody>
								{incomeComponents}
							</tbody>
						</Table>
					</>
				</Col>
				<Col xs={6} md={4}>
					<h3 className="text-center mb-5" style={{fontSize: 34}}>Income Form</h3>
					<Form onSubmit={e => addIncome(e)}>
					<Form.Group >
						<Form.Label>Category</Form.Label>
							<Form.Control as="select" placeholder="Select" value={category} onChange={e=>{setCategory(e.target.value)}}required>
								<option value="" disabled>Select</option>
								{categories}
							</Form.Control>	
					</Form.Group>
					<Form.Group>
						<Form.Label>Amount</Form.Label>
						<Form.Control type="number" placeholder="Enter Amount" value={amount} onChange={e=>{setAmount(e.target.value)}}required />
					</Form.Group>
					<Form.Group>
							<Form.Label>Type</Form.Label>
							<Form.Control as="select" placeholder="Select" value={type} onChange={e=>{setType(e.target.value)}} required>
								<option value="">Select</option>
								<option value="income">income</option>
							</ Form.Control>
					</Form.Group>
					{
						isActive
						?
						<Button variant="primary" type="submit">Submit</Button>
						:
						<Button variant="primary" type="submit" disabled>Submit</Button>
					}
				</Form>
				</Col>
			</Row>
		</>	
		:
		window.location.href="/login"	
	)		

}