import React,{useState,useEffect} from 'react'
import {Form, Button,Row,Col} from 'react-bootstrap'
import Swal from 'sweetalert2'

export default function AddCategories(){

	const [name,setName] = useState("")
	const [type,setType] = useState("")

	const [isActive, setIsActive]= useState(true)

	useEffect(()=>{

		if(name !== "" && type !== ""){

			setIsActive(true)

		} else {

			setIsActive(false)

		}

	},[name,type])

	function addCategory (e){

		e.preventDefault()

		fetch('https://nameless-brook-20939.herokuapp.com/api/categories/', {

			method: 'POST',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`,
				'Content-Type': 'application/json'
			},
			body:JSON.stringify({
				name: name,
				type: type	
			})

		})
		.then(res => res.json())
		.then(data => {

			console.log(data)
			Swal.fire({

			icon: "success",
			title: "Category is successfully added",

			})
		})

		setName("")
		setType("")

	}

	return (
		<>
			<div class="flex-container">
				<div id="addcategory">
					{/*<Row>*/}
						<h3 class="flex-item" className="text-center" style={{fontSize: 40}}>Add Category</h3>
					{/*</Row>*/}
					<Row className="justify-content-md-center">
						<Col xs={6} md={4} className="align-middle">
							<Form class="flex-item" onSubmit={e => addCategory(e)}>
							<Form.Group>
								<Form.Label>Category Name</Form.Label>
								<Form.Control type="text" placeholder="Enter Category Name" value={name} onChange={e=>{setName(e.target.value)}}required />
								</Form.Group>
								<Form.Group>
									<Form.Label>Type</Form.Label>
										<Form.Control as="select" placeholder="Select a type" value={type} onChange={e=>{setType(e.target.value)}} required>
											<option value="" disabled>Select a Type</option>
											<option value="income">income</option>
											<option value="expense">expense</option>
										</ Form.Control>
								</Form.Group>
								{
									isActive
									?
									<Button variant="primary" type="submit">Submit</Button>
									:
									<Button variant="primary" type="submit" disabled>Submit</Button>
								}
							</Form>
						</Col>	
					</Row>
				</div>		
			</div>
		</>
		)

}