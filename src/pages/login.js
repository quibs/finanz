import React,{useState,useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap'

import Swal from 'sweetalert2'

import {Redirect} from 'react-router-dom'

import UserContext from 'userContext'

export default function Login(){

	const {user,setUser} = useContext(UserContext)
	console.log(user)

	const [email,setEmail] = useState("")
	const [password,setPassword] = useState("")

	const [isActive, setIsActive]= useState(true)
	const [willRedirect,setWillRedirect] = useState(false)

	useEffect(()=>{

		if(email !== "" && password !== ""){

			setIsActive(true)

		} else {

			setIsActive(false)

		}

	},[email,password])

	function loginUser (e){

		e.preventDefault()

		fetch('https://nameless-brook-20939.herokuapp.com/api/users/login', {

			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body:JSON.stringify({
				email: email,
				password: password
			})

		})
		.then(res => res.json())
		.then(data => {

				console.log(data)
				if(data.accessToken){

				localStorage.setItem('token',data.accessToken)

				Swal.fire({

				icon: "success",
				title: "Logged In Successfully",
				text: "Thank you for logging in to Zuitt Booking System"

				})
				fetch('https://nameless-brook-20939.herokuapp.com/api/users',{

					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)
					
					localStorage.setItem('email',data.email)
					localStorage.setItem('isAdmin',data.isAdmin)

					setWillRedirect(true)

					setUser({
						email: data.email,
						isAdmin: data.isAdmin
					})

				})

			} else {

				Swal.fire({
					icon:"error",
					title:"Login failed"
				})

			}
		})

		setEmail("")
		setPassword("")	

	}


	return (
		willRedirect
		?
		<Redirect to="/"/>
		:
		<div id="login">
			<h3 className="text-center" style={{fontSize: 40}}>Login</h3>
			
			<Form onSubmit={e => loginUser(e)}>
				
				<Form.Group>
					<Form.Label>Email</Form.Label>
					<Form.Control type="email" placeholder="Enter Email" value={email} onChange={(e)=>setEmail(e.target.value)} required />
				</Form.Group>
				<Form.Group>
					<Form.Label>Password</Form.Label>
					<Form.Control type="password" placeholder="Enter Password" value={password} onChange={(e)=>setPassword(e.target.value)} required />
				</Form.Group>
				{
					isActive
					?
					<Button variant="primary" type="submit">Login</Button>
					:
					<Button variant="primary" type="submit" disabled>Login</Button>
				}
			</Form>
		</div>

	)
}