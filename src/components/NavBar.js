import React,{useState,useEffect,useContext} from 'react'

import {Navbar,Nav} from 'react-bootstrap'
import {Link,NavLink} from 'react-router-dom'

import UserContext from 'userContext'

export default function NavBar(){

	const {user} = useContext(UserContext)
	// console.log(user)

	const [expenses,setExpenses] = useState([])
	const [income,setIncome] = useState([])
	const [balance, setBalance] = useState (0)

		useEffect(()=>{

			if(user.email){
			fetch('https://nameless-brook-20939.herokuapp.com/api/entries/expenses',{

				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}

			})
			.then(res => res.json())
			.then(data => {

				// console.log(data)
				setExpenses(data.reduce(function(total,currentValue){
					return total + currentValue.amount
				},0))

			})
		}

		},[user])	
		// console.log(expenses)

		useEffect(()=>{

			if(user.email){
			fetch('https://nameless-brook-20939.herokuapp.com/api/entries/income',{

				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}

			})
			.then(res => res.json())
			.then(data => {

				// console.log(data)
				setIncome(data.reduce(function (total, currentValue){
					return total + currentValue.amount
				}, 0))

			})
		}

		},[user])	
		// console.log(income)

		useEffect(()=>{

			if(income-expenses !== 0)

				setBalance(income-expenses)

		},[income,expenses])




	return (

			<Navbar bg="light" expand="lg">
				<Navbar.Brand as={Link} to="/">Finanz</Navbar.Brand>
				<Navbar.Collapse className="justify-content-center">
					{
						user.email
						?
							<Navbar.Text style={{fontSize: 23,fontWeight: 'bold'}}>Current Balance: PHP {balance.toFixed(2)}</Navbar.Text>
						:
						null	
					}
				</Navbar.Collapse>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav" className="justify-content-end">
					{
						user.email
						?
						<>
							<Nav.Link as={NavLink} to="/addcategories" activeStyle={{ color: 'black' }}>Add Category</Nav.Link>
							<Nav.Link as={NavLink} to="/categories" activeStyle={{ color: 'black' }}>Category</Nav.Link>
							<Nav.Link as={NavLink} to="/expenses" activeStyle={{ color: 'black' }}>Expenses</Nav.Link>
							<Nav.Link as={NavLink} to="/income" activeStyle={{ color: 'black' }}>Income</Nav.Link>
							<Nav.Link as={NavLink} to="/logout" activeStyle={{ color: 'black' }}>Logout</Nav.Link>
						</>	
						:
						<>
							<Nav.Link as={NavLink} to="/login" activeStyle={{ color: 'black' }}>Login</Nav.Link>
							<Nav.Link as={NavLink} to="/register" activeStyle={{ color: 'black' }}>Register</Nav.Link>
						</>
					}
				</Navbar.Collapse>
			</Navbar>		


		)

}