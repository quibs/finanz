import React from 'react'

import {Card,Button,Row,Col} from 'react-bootstrap'

import Swal from 'sweetalert2'

export default function Category({category}) {

	const {name,type} = category

	console.log(category)

	function deleteCategory(e){

		console.log(category._id)

		e.preventDefault()

		fetch('http://localhost:4000/api/categories/' + category._id,{

			method: 'DELETE',
			headers: {
						Authorization: `Bearer ${localStorage.getItem('token')}`
					}

			})
			.then(res => res.json())
			.then(data => {

				console.log(data)

				Swal.fire({
			
				icon: "error",
				title: "deleted"

				})
				
				window.location.href="/categories"
			})
	
	}


	return (
		<Row>
			<Col xs={12} md={3}>
				<Card>
					<Card.Body>
						<Card.Title>
							{name}
						</Card.Title>
						<Card.Text>
							Type: {type}
						</Card.Text>
						<Button variant="danger" onClick={deleteCategory} id="deleteButton">Delete</Button>
					</Card.Body>
				</Card>
			</Col>
		</Row>	
	)

}