import React from 'react'
import {Jumbotron,Row,Col} from 'react-bootstrap'

import {Link} from 'react-router-dom'

export default function Banner({dataProp}){

	const {title,description,motto,label} = dataProp

	// console.log(dataProp)

	return (

		<>
			<Row id="jumbo">
				<Col>
					<Jumbotron className="text-center" id="jumboimage" fluid>
						<h1>{title}</h1>
						<p>{description}</p>
						<p>{motto}</p>
						<Link to="/register">{label}</Link>
					</Jumbotron>
				</Col>
			</Row>
		</>


		)


}